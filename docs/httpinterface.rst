The engine allows to load an HTTP server for configuration and retrieve information

If you decide to use the binary is the -a parameter

.. code:: bash 

   -a [ --port ] arg (=0)             Sets the HTTP listenting port.


Or if you want to decide to use PacketDispatcher object of the python binding use:

.. code:: python

    pd.http_port = 5008
    pd.authorized_ip_address = ["127.0.0.1"]

This allows to access to one running instance and interact and reprogram over an HTTP interface.

The available URIs on the server are:

- /aiengine/protocols/summary
- /aiengine/protocol
- /aiengine/flows
- /aiengine/summary
- /aiengine/system
- /aiengine/uris
- /aiengine/pcapfile
- /aiengine/python_code
- /aiengine/flow
- /aiengine/globals


/aiengine/uris
**************

This uri contains the available uris that the HTTP server provides.

.. code:: bash

   GET /aiengine/uris HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: text/html
   Content-Length: 720

   <html><head><title>AIEngine operations</title></head>
   <body>
   <a href="http://127.0.0.1:5008/aiengine/protocols/summary">Protocols summary</a><br>
   <a href="http://127.0.0.1:5008/aiengine/protocol">Protocol summary</a><br>
   <a href="http://127.0.0.1:5008/aiengine/flows">Network flows</a><br>
   <a href="http://127.0.0.1:5008/aiengine/summary">Summary</a><br>
   <a href="http://127.0.0.1:5008/aiengine/system">System</a><br>
   <a href="http://127.0.0.1:5008/aiengine/pcapfile">Upload pcapfile</a><br>
   <a href="http://127.0.0.1:5008/aiengine/python_code">Python code</a><br>
   <a href="http://127.0.0.1:5008/aiengine/globals">Python globals</a><br>
   <a href="http://127.0.0.1:5008/aiengine/flow">Network flow</a><br>
   </body>
   </html>


/aiengine/protocols/summary
***************************

.. code:: bash

   GET /aiengine/protocols/summary HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: text/plain
   Content-Length: 3899

   Protocol statistics summary
   ...

.. code:: bash

   GET /aiengine/protocols/summary HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: application/json
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: application/json
   Content-Length: 3054

   [
      {"bytes":655,"cache_memory":0,"events":0,"memory":968,"miss":0,"name":"Ethernet","packets":4,"used_memory":968},
      {"bytes":599,"cache_memory":0,"events":0,"memory":984,"miss":0,"name":"IP","packets":4,"used_memory":984},
      {"bytes":0,"cache_memory":0,"events":0,"memory":689216,"miss":0,"name":"TCP","packets":0,"used_memory":1088},
      {"bytes":519,"cache_memory":0,"events":0,"memory":287776,"miss":0,"name":"UDP","packets":4,"used_memory":1336},
   ...


/aiengine/flow
**************

The user can retrieve information about a specific TCP/UDP flow and also
modify some of the attributes while the engine is running.

.. code:: bash

   GET /aiengine/flow/[192.168.1.1:63139]:17:[192.168.1.254:53] HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: application/json
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: application/json
   Content-Length: 178

   {"bytes":40,
    "dns":{"domain":"s2.youtube.com","qtype":1},
    "evidence":false,
    "ip":{"dst":"192.168.1.254","src":"192.168.1.1"},
    "layer7":"dns",
    "port":{"dst":53,"src":63139},
    "proto":17}


Also modify some of the fields of the network flow

.. code:: bash

   PUT /aiengine/flow/[192.168.1.1:63139]:17:[192.168.1.254:53] HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1
   Content-Length: 45
   Content-Type: application/json

   {"label": "This is a lovely label my friend"}

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Length: 0


/aiengine/flows
***************

.. code:: bash

   GET /aiengine/flows HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1


.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: text/plain
   Content-Length: 380

   Flows on memory 1

   Flow                                                             Bytes      Packets    FlowForwarder      Info        
   Total 0

   Flow                                                             Bytes      Packets    FlowForwarder      Info        
   [10.0.2.15:51413]:17:[88.190.242.141:6881]                       519        4          UDPGenericProtocol
   Total 1


You can use the protocol name on the URI and filter them

.. code:: bash

   GET /aiengine/flows/http/1 HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: text/plain
   Content-Length: 274

   Flows on memory 1

   Flow                                                             Bytes      Packets    FlowForwarder      Info        
   Total 0

   Flow                                                             Bytes      Packets    FlowForwarder      Info        
   Total 0

/aiengine/protocol
******************

.. code:: bash

   GET /aiengine/protocol/dns HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: text/plain
   Content-Length: 184

   DNSProtocol(0x5611c9823cf0) statistics
        Dynamic memory alloc:           no
        Total allocated:         73 KBytes
        Total packets:                   0
	Total bytes:                     0


.. code:: bash

   GET /aiengine/protocol/dns/5 HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: text/plain
   Content-Length: 1656

   DNSProtocol(0x5611c9823cf0) statistics
	Dynamic memory alloc:           no
	Total allocated:         73 KBytes
	Total packets:                   0
	Total bytes:                     0
	Total valid packets:             0
	Total invalid packets:           1
	Total allow queries:             0
	Total banned queries:            0
	Total queries:                   0
	Total responses:                 0
	Total type A:                    0
	Total type NS:                   0
	Total type CNAME:                0
	Total type SOA:                  0
	Total type PTR:                  0
	Total type MX:                   0
	Total type TXT:                  0
	Total type AAAA:                 0
	Total type LOC:                  0
	Total type SRV:                  0
	Total type DS:                   0
	Total type SSHFP:                0
	Total type DNSKEY:               0
	Total type IXFR:                 0
	Total type ANY:                  0
	Total type others:               0
   FlowForwarder(0x5611c97113b0) statistics
	Plugged to object(0x5611c9823cf0)
	Total forward flows:             0
	Total received flows:            0
	Total fail flows:                0
   DNS Info cache statistics
	Total items:                   512
	Total allocated:         44 KBytes
	Total current alloc:     44 KBytes
	Total acquires:                  0
	Total releases:                  0
	Total fails:                     0
   Name cache statistics
	Total items:                   512
	Total allocated:         28 KBytes
	Total current alloc:     28 KBytes
	Total acquires:                  0
	Total releases:                  0
	Total fails:                     0
	DNS Name usage

.. code:: bash

   GET /aiengine/protocol/dns/5 HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: application/json
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: application/json
   Content-Length: 337

   {"allocated_bytes":75056,
    "allow_queries":0,
    "banned_queries":0,
    "bytes":0,
    "dynamic_memory":false,
    "invalid_packets":1,
    "name":"DNSProtocol",
    "packets":0,
    "queries":0,
    "responses":0,
    "types":{"a":0,
        "aaaa":0,
        "any":0,
        "cname":0,
        "dnskey":0,
        "ds":0,
        "ixfr":0,
        "loc":0,
        "mx":0,
        "ns":0,
        "others":0,
        "ptr":0,
        "soa":0,
        "srv":0,
        "sshfp":0,
        "txt":0},
    "valid_packets":0}

.. code:: bash

   GET /aiengine/protocol/http/map/hosts HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: application/json
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: application/json
   Content-Length: 20

   {"www.google.com":1}


/aiengine/system
****************

.. code:: bash

   GET /aiengine/system HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: application/json
   User-Agent: python-requests/2.19.1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: application/json
   Content-Length: 316

   {"elapsed_time":"00:00:07.355174",
    "lock_memory":false,
    "machine":"x86_64",
    "nodename":"vmfedora25",
    "pid":10865,
    "release":"5.0.16-100.fc28.x86_64",
    "resident_memory":26768,
    "shared_memory":0,
    "sysname":"Linux",
    "unshared_data":0,
    "unshared_stack":0,
    "version":"#1 SMP Tue May 14 18:22:28 UTC 2019",
    "virtual_memory":411856896}

/aiengine/pcapfile 
******************

Now is possible to upload pcap files to the engine for analisys

.. code:: bash

   POST /aiengine/pcapfile HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1
   Content-Length: 3323
   Content-Type: multipart/form-data; boundary=80ff982a95a2aa44cfd13c2b9ac390e9

   --80ff982a95a2aa44cfd13c2b9ac390e9
   Content-Disposition: form-data; name="file"; filename="accessgoogle.pcap"

   ........................q=.Q.q..J...J...$v}9.q...IC\..E..<^.@.@. ....
   Ye.....5.(AI.............www.google.com.....q=.Q.q..J...J...$v}9.q...IC\..E..<^.@.@. ....
   Ye.....5.(...............www.google.com.....q=.Q...............IC\$v}9.q..E.....@.;..1Ye.....
   ...

/aiengine/python_code
*********************

Is possible to send python code directly to the engine in order to modify the behavior


.. code:: bash

   POST /aiengine/python_code HTTP/1.1
   Host: 127.0.0.1:5008
   Connection: keep-alive
   Accept-Encoding: gzip, deflate
   Accept: */*
   User-Agent: python-requests/2.19.1
   Content-Type: text/python
   Content-Length: 18

   a = 1 + 5
   print(a)

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Length: 2

   6

/aiengine/globals
*****************

When the engine is running with the python binding is possible to retrieve the variables loaded on the server.
This allows the user to reprogram the instance as he wants depending on what have that instance loaded on memory.

.. code:: bash

   GET /aiengine/globals HTTP/1.1
   Host: 127.0.0.1:8080
   User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0
   Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
   Accept-Language: en-US,en;q=0.5
   Accept-Encoding: gzip, deflate
   Referer: http://127.0.0.1:8080/aiengine/uris
   Connection: keep-alive
   Upgrade-Insecure-Requests: 1

.. code:: bash

   HTTP/1.1 200 OK
   Server: AIEngine 1.9.1
   Content-Type: text/plain
   Content-Length: 200

   Python objects
	pcapfile:str
	__builtins__:module
	__file__:str
	__package__:NoneType
	sys:module
	pyaiengine:module
	pd:PacketDispatcher
	__name__:str
	rm:RegexManager
	st:StackLan
	__doc__:NoneType



