#!/usr/bin/env python

""" Example for detect denial of service attacks """

__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright (C) 2013-2020 by Luis Campo Giralte"
__revision__ = "$Id$"
__version__ = "0.1"

import sys
sys.path.append("../src/")
import pyaiengine

stack = None

def scheduler_handler_tcp():

    print("TCP DoS Checker")
    counters = stack.get_counters("TCPProtocol")

    # Code the intelligence for detect DDoS based on
    # combination flags, bytes, packets and so on.
    syns = int(counters["syns"])
    synacks = int(counters["synacks"])
    if syns > (synacks * 100):
        print("System under a SYN DoS attack")

    # Check also if there is no ack packets or whatever

def scheduler_handler_ntp():

    total_ips = dict()
    print("NTP DDoS Checker")

    # Count the number different ips of the NTP flows
    for flow in stack.udp_flow_manager:
        if flow.l7_protocol_name == "NTPProtocol":
            total_ips[flow.src_ip] = 1

    if len(total_ips) == len(stack.udp_flow_manager):
        print("System under a NTP DDoS attack")
 
if __name__ == '__main__':

    # Load an instance of a Network Stack Lan
    stack = pyaiengine.StackLan()

    stack.tcp_flows = 327680
    stack.udp_flows = 163840

    # Create a instace of a PacketDispatcher
    with pyaiengine.PacketDispatcher("ens7") as pd:
        pd.stack = stack
        # Sets a handler method that will be call
        # every 5 seconds for check the values
        pd.add_timer(scheduler_handler_tcp, 5)
        pd.run()

    sys.exit(0)
