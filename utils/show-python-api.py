"""Shows the python api for aiengine """
__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright (C) 2013-2019 by Luis Campo Giralte"
__revision__ = "$Id$"
__version__ = "0.1"
import sys
import inspect
sys.path.append("../src/")
import pyaiengine

def show_plain_text(name):
    print(name.__name__)
    a = inspect.getdoc(name)
    if a:
        print("  ", a)
    for m in dir(name):
        if (not m.startswith("__")):
            item = getattr(name, m)
            if type(item).__module__ == "Boost.Python":
                output = ""
                for line in inspect.getdoc(item).split("\n"):
                    if not line.startswith(m) and line:
                        output = line + "\n"

                attr_help = output.rstrip().lstrip()
            else:
                attr_help = inspect.getdoc(item)
            print("   |--->%s. %s" % (m, attr_help))

def show_restructured_text(name):
    print("* **%s**\n" % name.__name__)
    a = inspect.getdoc(name)
    if a:
        print("  %s\n" % a)
    methods = []
    properties = []
    for m in dir(name):
        if (not m.startswith("__")):
            item = getattr(name, m)
            if type(item).__module__ == "Boost.Python":
                output = ""
                for line in inspect.getdoc(item).split("\n"):
                    if not line.startswith(m) and line:
                        output = line + "\n"
                        break

                attr_help = output.rstrip().lstrip()
                methods.append("%s. %s" % (m, attr_help))
            else:
                attr_help = inspect.getdoc(item)
                properties.append("%s. %s" % (m, attr_help))

    if methods:
        print("  * Methods\n")
        for method in methods:
            print("    * %s" % method)
        print("\n")

    if properties:
        print("  * Properties\n")
        for prop in properties:
            print("    * %s" % prop)
        print("\n")

if __name__ == '__main__':

    for mod in dir(pyaiengine):
        name = pyaiengine.__dict__.get(mod)
        if ((inspect.isclass(name))and(type(name).__module__=="Boost.Python")):
            # show_plain_text(name)
            show_restructured_text(name)

    sys.exit(0)
