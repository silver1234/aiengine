/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2020  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "SSHInfo.h"

namespace aiengine {

void SSHInfo::reset() { 

	total_encrypted_bytes_ = 0;
	is_client_handshake_ = true;
	is_server_handshake_ = true;
	client_name.reset();
	server_name.reset();
}

std::ostream& operator<< (std::ostream &out, const SSHInfo &info) {

	out << " Handshake:" << (info.isHandshake() ? "True" : "False"); 

	if (info.client_name) 
		out << " Client:" << info.client_name->getName();

	if (info.server_name) 
		out << " Server:" << info.server_name->getName();

	if (info.isHandshake() == false)
		out << " CBytes:" << info.total_encrypted_bytes_;

        return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const SSHInfo &info) {

	out["handshake"] = info.isHandshake(); 
	out["crypt_bytes"] = info.total_encrypted_bytes_;

	if (info.client_name) 
		out["clientname"] = info.client_name->getName();

	if (info.server_name) 
		out["servername"] = info.server_name->getName();

        return out;
}

} // namespace aiengine
