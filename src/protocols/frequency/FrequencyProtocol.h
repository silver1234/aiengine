/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2020  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_FREQUENCY_FREQUENCYPROTOCOL_H_
#define SRC_PROTOCOLS_FREQUENCY_FREQUENCYPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <cstring>
#include "Protocol.h"
#include "Frequencies.h"
#include "PacketFrequencies.h"
#include "Cache.h"
#include "flow/FlowManager.h"

namespace aiengine {

class FrequencyProtocol: public Protocol {
public:
	explicit FrequencyProtocol(const std::string& name, uint16_t protocol_layer);
	explicit FrequencyProtocol():FrequencyProtocol("Frequency", 0) {}
    	virtual ~FrequencyProtocol() {}
	
	static const int header_size = 2;
	static const int DefaultInspectionLimit = 128; // Number of packets process for compute the frequencies

	uint16_t getId() const override { return 0x0000; }
	int getHeaderSize() const override { return header_size;}

	// All the flows are processed by the frequency proto
	bool check(const Packet &packet) override; 
	bool processPacket(Packet& packet) override { return true; }
	void processFlow(Flow *flow) override;

	void statistics(std::basic_ostream<char>& out, int level) const override;
	void statistics(Json &out, int level) const override;

	void releaseCache() override; 

        void setHeader(const uint8_t *raw_packet) override {
        
                freq_header_ = raw_packet;
        }

        void increaseAllocatedMemory(int value) override;
        void decreaseAllocatedMemory(int value) override;

	void releaseFlowInfo(Flow *flow) override;
	
	void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	int64_t getCurrentUseMemory() const override;
	int64_t getAllocatedMemory() const override; 
	int64_t getTotalAllocatedMemory() const override; 

        void setDynamicAllocatedMemory(bool value) override;
	bool isDynamicAllocatedMemory() const override;

	CounterMap getCounters() const override;
	void resetCounters() override;

private:
	const uint8_t *freq_header_ = nullptr;
	int inspection_limit_ = DefaultInspectionLimit;
	Cache<Frequencies>::CachePtr freqs_cache_ = Cache<Frequencies>::CachePtr(new Cache<Frequencies>("Frequencies cache"));
	Cache<PacketFrequencies>::CachePtr packet_freqs_cache_ = Cache<PacketFrequencies>::CachePtr(new Cache<PacketFrequencies>("Packet frequencies cache"));
	FlowManagerPtrWeak flow_mng_ = FlowManagerPtrWeak();
};

typedef std::shared_ptr<FrequencyProtocol> FrequencyProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_FREQUENCY_FREQUENCYPROTOCOL_H_
