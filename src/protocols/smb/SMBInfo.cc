/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2020  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "SMBInfo.h"

namespace aiengine {

void SMBInfo::reset() {

	filename.reset();
	command_ = 0;
}

std::ostream& operator<< (std::ostream &out, const SMBInfo &info) {

	out << " CMD:" << info.command_;
        if (info.filename)
                out << " File:" <<  info.filename->getName();

	return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const SMBInfo &info) {

	out["cmd"] = info.command_;
        if (info.filename)
                out["filename"] = info.filename->getName();

	return out;
}

} // namespace aiengine

