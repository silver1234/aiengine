/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2020  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "SSLInfo.h"

namespace aiengine {

void SSLInfo::reset() {

	host_name.reset();
	issuer.reset();
	session_id.reset();
#if defined(HAVE_JA3)
	ja3_fingerprint.reset();
#endif
	matched_domain_name.reset();
	is_banned_ = false;
	encrypted_ = false;
	data_pdus_ = 0;
	version_ = 0;
	heartbeat_ = false;
	alert_ = false;
	alert_code_ = 0;
	cipher_ = 0;
}

std::ostream& operator<< (std::ostream &out, const SSLInfo &info) {

	out << " Pdus:" << info.getTotalDataPdus();
	out << " Cipher:0x" << std::hex << info.cipher_;
	out << " Ver:0x" << std::hex << info.version_ << std::dec;

	if (info.isBanned())
		out << " Banned";

	if (info.host_name)
		out << " Host:" << info.host_name->getName();

	if (info.issuer)
		out << " Issuer:" << info.issuer->getName();

	if (info.session_id)
		out << " Session:" << info.session_id->getName();
#if defined(HAVE_JA3)
	if (info.ja3_fingerprint)
		out << " Fingerprint:" << info.ja3_fingerprint->getName();
#endif
	return out;
}

nlohmann::json& operator<< (nlohmann::json& out, const SSLInfo &info) {

        out["pdus"] = info.getTotalDataPdus();
        out["cipher"] = info.cipher_;
        out["version"] = info.version_;

	if (info.alert_)
		out["alert"] = info.alert_code_;

	if (info.heartbeat_)
		out["heartbeat"] = true;

        if (info.isBanned())
		out["banned"] = true;

        if (info.host_name)
		out["host"] = info.host_name->getName();

        if (info.issuer)
		out["issuer"] = info.issuer->getName();

        if (info.session_id)
		out["session"] = info.session_id->getName();

	if (info.matched_domain_name)
		out["matchs"] = info.matched_domain_name->getName();
#if defined(HAVE_JA3)
        if (info.ja3_fingerprint)
		out["fingerprint"] = info.ja3_fingerprint->getName();
#endif
        return out;
}

} // namespace aiengine
