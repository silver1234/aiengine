#!/bin/bash

GO=/usr/bin/go
SWIG=/usr/bin/swig
GOPATHDIR=gopath/src/goaiengine
export CGO_CPPFLAGS="-I `pwd` -I `pwd`/../"
export CGO_CXXFLAGS="-DHAVE_CONFIG_H -DBINDING -DGO_BINDING -std=c++17"
export CGO_LDFLAGS="-lstdc++ -lboost_system -lboost_iostreams -lboost_log -lboost_thread -lm -lpcap -lpcre";
export GOPATH=`pwd`/gopath
if [ ! -f goaiengine.a ]; then
    (cd $GOPATHDIR && go build -x -o goaiengine.a )
    cp $GOPATHDIR/goaiengine.a .
fi
echo "Compiling Go extension"
$GO tool compile -pack -o goai.o goai.go
echo "Linking Go extension"
$GO tool link -linkmode external -extld "g++" -extldflags "-I/usr/include" -o goai_test goai.o
echo "Compilation sucess"
exit

