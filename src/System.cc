/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2020  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "System.h"
#include "UnitConverter.h"

#if defined(IS_DARWIN)
#include <mach/mach.h>
#endif

namespace aiengine {

boost::posix_time::ptime System::start_time_ = boost::posix_time::microsec_clock::local_time();

System::System():
	current_time_(boost::posix_time::microsec_clock::local_time()),
	is_memory_lock_(false)
	{

	uname(&system_info_),
	getrusage(RUSAGE_SELF,&usage_);
}

System::~System() {

	munlockall();
}

void System::statistics(std::basic_ostream<char> &out) {

	struct rusage usage;
	std::ostringstream proc_file;
	int64_t virtual_memory = 0;
	int64_t resident_memory = 0;
	std::string unit("Bytes");
	std::string runit("Bytes");

#if defined(IS_LINUX)
	proc_file << "/proc/" << getpid() << "/stat";

	std::vector<std::string> items;
	try {
		std::string item;
		std::ifstream fd (proc_file.str());
		while(std::getline(fd, item, ' ')) {
			items.push_back(item);
    		}
		// The virtual memory is on the 22 index value
		virtual_memory = std::stoi(items.at(22));
	} catch ( ... ) { /* LCOV_EXCL_LINE */

	}
#elif defined(IS_DARWIN)
	mach_msg_type_number_t count = MACH_TASK_BASIC_INFO_COUNT;
	mach_task_basic_info_data_t taskinfo;

	if (task_info(mach_task_self(), MACH_TASK_BASIC_INFO, (task_info_t)&taskinfo, &count) == KERN_SUCCESS)
		virtual_memory = taskinfo.virtual_size;
#endif
	unitConverter(virtual_memory, unit);

	getrusage(RUSAGE_SELF,&usage);

	resident_memory = usage.ru_maxrss;
	unitConverter(resident_memory, runit);

	current_time_ = boost::posix_time::microsec_clock::local_time();
	boost::posix_time::time_duration duration(current_time_ - start_time_);

        out << "System process statistics" << std::dec <<  "\n";
        out << "\t" << "Elapsed time:      " << duration << "\n";
        out << "\t" << "Virtual memory size:    " << std::setw(9 - unit.length()) << virtual_memory << " " << unit << "\n";
        out << "\t" << "Lock memory:                 " << std::setw(5) << (is_memory_lock_ ? "yes":"no") << "\n";
        out << "\t" << "Resident memory size:   " << std::setw(9 - runit.length()) << resident_memory << " " << runit << "\n";
        out << "\t" << "Shared memory size:          " << std::setw(5) << usage.ru_ixrss << "\n";
        out << "\t" << "Unshared data size:          " << std::setw(5) << usage.ru_idrss << "\n";
        out << "\t" << "Unshared stack size:         " << std::setw(5) << usage.ru_isrss << "\n";
        out << "\t" << "Page reclaims:             " << std::setw(7) << usage.ru_minflt << "\n";
        out << "\t" << "Page faults:                 " << std::setw(5) << usage.ru_majflt << "\n";
        out << "\t" << "Swaps:                       " << std::setw(5) << usage.ru_nswap << "\n";
        out << "\t" << "Block input operations: " << std::setw(10) << usage.ru_inblock << "\n";
        out << "\t" << "Block output operations:     " << std::setw(5) << usage.ru_oublock << "\n";
        out << "\t" << "IPC messages sent:           " << std::setw(5) << usage.ru_msgsnd << "\n";
        out << "\t" << "IPC messages received:       " << std::setw(5) << usage.ru_msgrcv << "\n";
        out << "\t" << "Signal received:             " << std::setw(5) << usage.ru_nsignals << "\n";
        out << "\t" << "Voluntary context switches:" << std::setw(7) << usage.ru_nvcsw << "\n";
        out << "\t" << "Involuntary context switches:" << std::setw(5) << usage.ru_nivcsw << std::endl;
}

std::string System::getOSName() const {
	std::ostringstream os;

        os << system_info_.sysname;
	return os.str();
}

std::string System::getNodeName() const {
	std::ostringstream os;

        os << system_info_.nodename;
	return os.str();
}

std::string System::getReleaseName() const {
	std::ostringstream os;

        os << system_info_.release;
	return os.str();
}

std::string System::getVersionName() const {
	std::ostringstream os;

        os << system_info_.version;
	return os.str();
}

std::string System::getMachineName() const {
	std::ostringstream os;

        os << system_info_.machine;
	return os.str();
}

void System::statistics(Json &out) {

	struct rusage usage;
	std::ostringstream proc_file, elapsed_str;
	int64_t virtual_memory = 0;

	out["sysname"] = system_info_.sysname;
	out["nodename"] = system_info_.nodename;
	out["release"] = system_info_.release;
	out["version"] = system_info_.version;
	out["machine"] = system_info_.machine;

	out["pid"] = getpid();

	proc_file << "/proc/" << getpid() << "/stat";

	std::vector<std::string> items;
	try {
		std::string item;
		std::ifstream fd (proc_file.str());
		while(std::getline(fd, item, ' ')) {
			items.push_back(item);
    		}
		// The virtual memory is on the 22 index value
		virtual_memory = std::stoi(items.at(22));
	} catch ( ... ) { /* LCOV_EXCL_LINE */

	}

	getrusage(RUSAGE_SELF,&usage);

	current_time_ = boost::posix_time::microsec_clock::local_time();
	boost::posix_time::time_duration duration(current_time_ - start_time_);

	elapsed_str << duration;
	out["elapsed_time"] = elapsed_str.str();
	out["virtual_memory"] = virtual_memory;
	out["lock_memory"] = is_memory_lock_;
	out["resident_memory"] = usage.ru_maxrss;
	out["shared_memory"] = usage.ru_ixrss;
	out["unshared_data"] = usage.ru_idrss;
	out["unshared_stack"] = usage.ru_isrss;
}

} // namespace aiengine
