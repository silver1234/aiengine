/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2020  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com>
 *
 */
#include "PacketDispatcher.h"
#include <boost/exception/diagnostic_information.hpp>
#include <boost/exception_ptr.hpp>

namespace aiengine {

PacketDispatcher::PacketDispatcher(const std::string &source, int buffer_size):
	io_service_(),
        signals_(io_service_, SIGINT, SIGTERM),
	input_name_(source),
	pcap_buffer_size_(buffer_size) {

        signals_.async_wait(boost::bind(&boost::asio::io_service::stop, &io_service_));
}

PacketDispatcher::~PacketDispatcher() {

	// The object can be destroy with a stack attached
	if (current_network_stack_)
		current_network_stack_->stopAsioService();

	io_service_.stop();
}

bool PacketDispatcher::is_ethernet_present() const {

	if (pcap_) {
		int ret = pcap_datalink(pcap_);
		if (ret == DLT_EN10MB)
			return true;
	}
	return false;
}


void PacketDispatcher::info_message(const std::string &msg) {

	aiengine::information_message(msg);
}

void PacketDispatcher::error_message(const std::string &msg) {

	aiengine::error_message(msg);
}

void PacketDispatcher::statistics() {

        statistics(OutputManager::getInstance()->out());
}

void PacketDispatcher::set_stack(const SharedPointer<NetworkStack> &stack) {

        current_network_stack_ = stack;
	stack_name_ = stack->getName();
        setDefaultMultiplexer(stack->getLinkLayerMultiplexer().lock());
        stack->setAsioService(io_service_);
}

void PacketDispatcher::setStack(const SharedPointer<NetworkStack> &stack) {

	set_stack(stack);
}

void PacketDispatcher::setDefaultMultiplexer(MultiplexerPtr mux) {

	defMux_ = mux;
	auto proto = mux->getProtocol();
	eth_ = std::dynamic_pointer_cast<EthernetProtocol>(proto);
}

int PacketDispatcher::get_mtu_of_network_device(const std::string &name) {

	if (name.compare("any") == 0)
		return ETHER_MAX_LEN;

	struct ifreq ifr;
        int fd = socket(AF_INET, SOCK_DGRAM, 0);

	if (fd != -1) {
        	ifr.ifr_addr.sa_family = AF_INET;
        	strncpy(ifr.ifr_name , name.c_str() , IFNAMSIZ - 1);

		if (ioctl(fd, SIOCGIFMTU, &ifr) == 0) {
			// Use the global namespace for link with the system call close
			::close(fd);
			return ifr.ifr_mtu;
		}
		::close(fd);
	}

        std::ostringstream msg;
        msg << "Can not get MTU of device:" << input_name_.c_str();

	AIWARN << msg.str();

        error_message(msg.str());
	return 0;
}

void PacketDispatcher::open_device(const std::string &device, int buffer_size) {

	std::ostringstream msg;
	char errorbuf[PCAP_ERRBUF_SIZE];
#if defined(IS_FREEBSD) || defined(IS_DARWIN)
	int timeout = 1000; // miliseconds
#else
	int timeout = -1;
#endif

	if (pcap_ = pcap_create(device.c_str(), errorbuf); pcap_ != nullptr) {
		int status;

		if ((status = pcap_set_timeout(pcap_, timeout)) != 0)
			msg << device << ": pcap_set_timeout failed:" << pcap_statustostr(status);

		if ((status = pcap_set_snaplen(pcap_, PACKET_RECVBUFSIZE)) != 0) {
			msg.clear();
			msg << device << ": Can't set snapshot length:" << pcap_statustostr(status);
		}

		if ((status = pcap_set_buffer_size(pcap_, buffer_size)) != 0) {
			msg.clear();
			msg << device << ": Can't set buffer size:" << pcap_statustostr(status);
		}

		if ((status = pcap_activate(pcap_)) == 0) {

			int ifd = pcap_get_selectable_fd(pcap_);
			if (pcap_setnonblock(pcap_, 1, errorbuf) == 1) {
				msg.clear();
				msg << device << ": pcap_setnonblock failed:" << errorbuf;
			} else {
				stream_ = PcapStreamPtr(new PcapStream(io_service_));

				// just update the variable if all is good
				pcap_buffer_size_ = buffer_size;
				stream_->assign(::dup(ifd));
				device_is_ready_ = true;
				input_name_ = device;
				return;
			}
		} else {
			msg.clear();
			msg << device << ": " << pcap_statustostr(status) << ":" << pcap_geterr(pcap_);
		}
	} else {
		msg << "Device:" << device.c_str() << " error:" << errorbuf;
	}

	AIWARN << msg.str();

	error_message(msg.str());

	device_is_ready_ = false;
}

void PacketDispatcher::close_device(void) {

	if (device_is_ready_) {
		stream_->close();
		pcap_close(pcap_);
		device_is_ready_ = false;
	}
}

void PacketDispatcher::open_pcap_file(const std::string &filename) {

	char errorbuf[PCAP_ERRBUF_SIZE];

        if (pcap_ = pcap_open_offline(filename.c_str(), errorbuf); pcap_ == nullptr) {
		pcap_file_ready_ = false;
        	std::ostringstream msg;
		msg << "Unkown pcapfile:" << filename.c_str();

		AIWARN << msg.str();

		error_message(msg.str());
	} else {
		pcap_file_ready_ = true;
		input_name_ = filename;
	}
}

void PacketDispatcher::close_pcap_file(void) {

	if (pcap_file_ready_) {
		pcap_close(pcap_);
		pcap_file_ready_ = false;
	}
}

void PacketDispatcher::read_raw_network(boost::system::error_code ec) {

	if (pcap_next_ex(pcap_, &packet_header_, &packet_) >= 0) {
		packet_length_ = packet_header_->len;
		forward_current_raw_packet();
	}

// This prevents a problem on the boost asio signal
// remove this if when boost will be bigger than 1.50
#ifdef PYTHON_BINDING
#if BOOST_VERSION >= 104800 && BOOST_VERSION < 105000
	if (PyErr_CheckSignals() == -1) {
		std::ostringstream msg;
		msg << "Throwing exception from python";

		error_message(msg.str());
		throw std::runtime_error("Python exception\n");
	}
#endif
#endif

	if (!ec || ec == boost::asio::error::would_block)
		start_read_raw_network();
}

void PacketDispatcher::read_ethernet_network(boost::system::error_code ec) {

	if (pcap_next_ex(pcap_, &packet_header_, &packet_) >= 0) {
		packet_length_ = packet_header_->len;
		forward_current_ethernet_packet();
	}

// This prevents a problem on the boost asio signal
// remove this if when boost will be bigger than 1.50
#ifdef PYTHON_BINDING
#if BOOST_VERSION >= 104800 && BOOST_VERSION < 105000
	if (PyErr_CheckSignals() == -1) {
		std::ostringstream msg;
		msg << "Throwing exception from python";

		error_message(msg.str());
		throw std::runtime_error("Python exception\n");
	}
#endif
#endif

	if (!ec || ec == boost::asio::error::would_block)
		start_read_ethernet_network();
}

void PacketDispatcher::forward_current_ethernet_packet() {

	++total_packets_;
	total_bytes_ += packet_length_;

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
	stats_.packet_time = packet_header_->ts.tv_sec;
#endif
	if (defMux_) {
		current_packet_.setPayload(packet_);
		current_packet_.setPayloadLength(packet_length_);
		current_packet_.setPrevHeaderSize(0);
		current_packet_.setPacketTime(packet_header_->ts.tv_sec);
		current_packet_.setEvidence(false);

		if (defMux_->accept(current_packet_)) {
			defMux_->setPacket(&current_packet_);
			defMux_->setNextProtocolIdentifier(eth_->getEthernetType());
			defMux_->forward(current_packet_);

			if ((have_evidences_)and(current_packet_.haveEvidence()))
				em_->write(current_packet_);
                }
	}
}

void PacketDispatcher::forward_current_raw_packet() {

	++total_packets_;
	total_bytes_ += packet_length_;

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
	stats_.packet_time = packet_header_->ts.tv_sec;
#endif
	if (defMux_) {
		current_packet_.setPayload(packet_);
		current_packet_.setPayloadLength(packet_length_);
		current_packet_.setPrevHeaderSize(0);
		current_packet_.setPacketTime(packet_header_->ts.tv_sec);
		current_packet_.setEvidence(false);

		if (defMux_->accept(current_packet_)) {
			defMux_->setPacket(&current_packet_);
			defMux_->setNextProtocolIdentifier(current_network_stack_->getLinkLayerType());
			defMux_->forward(current_packet_);

			if ((have_evidences_)and(current_packet_.haveEvidence()))
				em_->write(current_packet_);
                }
	}
}

void PacketDispatcher::start_read_raw_network(void) {

	read_in_progress_ = false;
	if (!read_in_progress_) {
		read_in_progress_ = true;

		stream_->async_read_some(boost::asio::null_buffers(),
			boost::bind(&PacketDispatcher::read_raw_network, this,
                                boost::asio::placeholders::error));
	}
}

void PacketDispatcher::start_read_ethernet_network(void) {

	read_in_progress_ = false;
	if (!read_in_progress_) {
		read_in_progress_ = true;

		stream_->async_read_some(boost::asio::null_buffers(),
                	boost::bind(&PacketDispatcher::read_ethernet_network, this,
                                boost::asio::placeholders::error));
	}
}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
void PacketDispatcher::start_read_user_input(void) {

	user_shell_->readUserInput();
}

#endif

#if defined(STAND_ALONE_TEST) || defined(TESTING)
void PacketDispatcher::setMaxPackets(int packets) {

	max_packets_ = packets;
}

void PacketDispatcher::setFromPackets(int packets) {

	from_packets_ = packets;
}
#endif

void PacketDispatcher::run_pcap(void) {

        std::ostringstream msg;
        msg << "Processing packets from file " << input_name_.c_str();
       	info_message(msg.str());

	AIINFO << msg.str();

	if (eth_)
		eth_->setMaxEthernetLength(ETHER_MAX_LEN * 4); // Increase the size to a big value probably 65243 is the best

	if (current_network_stack_) {
		int64_t memory = current_network_stack_->getAllocatedMemory();
		std::string unit = "Bytes";

		unitConverter(memory,unit);

		msg.clear();
		msg.str("");
        	msg << "Stack '" << stack_name_ << "' using " << memory << " " << unit << " of memory";
       		info_message(msg.str());
        } else {
                msg.clear();
                msg.str("");
                msg << "No stack configured";
                warning_message(msg.str());
        }

#if defined(STAND_ALONE_TEST) || defined(TESTING)
	int total_exclude_packets = 0;
#endif
	status_ = PacketDispatcherStatus::RUNNING;

	if (is_ethernet_present()) {
		if (defMux_)
			defMux_->setHeaderSize(ETHER_HDR_LEN);
		while (pcap_next_ex(pcap_, &packet_header_, &packet_) >= 0) {
			// Friendly remminder:
			//     header_->len contains length this packet (off wire)
			//     header_->caplen length of portion present

#if defined(STAND_ALONE_TEST) || defined(TESTING)
			if (total_exclude_packets < from_packets_) {
				++total_exclude_packets;
				continue;
			}
#endif
			packet_length_ = packet_header_->caplen;

			forward_current_ethernet_packet();

#if defined(STAND_ALONE_TEST) || defined(TESTING)
     			if (total_packets_ >= max_packets_)
				break;
#endif
		}
	} else {
		if (defMux_)
			defMux_->setHeaderSize(16);

		while (pcap_next_ex(pcap_, &packet_header_, &packet_) >= 0) {
			// Friendly remminder:
			//     header_->len contains length this packet (off wire)
			//     header_->caplen length of portion present

#if defined(STAND_ALONE_TEST) || defined(TESTING)
			if (total_exclude_packets < from_packets_) {
				++total_exclude_packets;
				continue;
			}
#endif
			packet_length_ = packet_header_->caplen;

			forward_current_raw_packet();

#if defined(STAND_ALONE_TEST) || defined(TESTING)
     			if (total_packets_ >= max_packets_)
				break;
#endif
		}
	}

	status_ = PacketDispatcherStatus::STOPPED;
}

void PacketDispatcher::run_device(void) {

	if (device_is_ready_) {
        	std::ostringstream msg;
        	msg << "Processing packets from device " << input_name_.c_str();

        	info_message(msg.str());
		AIINFO << msg.str();

        	if ((current_network_stack_)&&(eth_)) {
                	int64_t memory = current_network_stack_->getAllocatedMemory();
			eth_->setMaxEthernetLength(get_mtu_of_network_device(input_name_));

                	std::string unit = "Bytes";

                	unitConverter(memory,unit);

                	msg.clear();
                	msg.str("");
                	msg << "Stack '" << stack_name_ << "' using " << memory << " " << unit << " of memory";
                	info_message(msg.str());
        	} else {
                	msg.clear();
                	msg.str("");
                	msg << "No stack configured";
                	warning_message(msg.str());
		}

		AIINFO << msg.str();

		try {
			status_ = PacketDispatcherStatus::RUNNING;
			if (is_ethernet_present()) {
				if (defMux_)
					defMux_->setHeaderSize(ETHER_HDR_LEN);
				start_read_ethernet_network();
			} else {
				if (defMux_)
#if defined(IS_FREEBSD) || defined(IS_DARWIN)
					defMux_->setHeaderSize(4);
#else
					defMux_->setHeaderSize(16);
#endif
				start_read_raw_network();
			}
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
			start_read_user_input();
#endif
			io_running_ = true;
			io_service_.run();
		} catch (const std::exception& e) {
        		std::ostringstream msg;
        		msg << ":ERROR:" << e.what() << std::endl;
			msg << boost::diagnostic_information(e);

			AIERROR << msg.str();
			error_message(msg.str());
        	}
		status_ = PacketDispatcherStatus::STOPPED;
	} else {

                std::ostringstream msg;
                msg << "The device is not ready to run";

		AIINFO << msg.str();
                info_message(msg.str());
	}
}

void PacketDispatcher::open(const std::string &source) {

	open(source, pcap_buffer_size_);
}

void PacketDispatcher::open(const std::string &source, int buffer_size) {

	std::ifstream infile(source);

	device_is_ready_ = false;
	pcap_file_ready_ = false;

	if (infile.good()) { // The source is a file
		open_pcap_file(source);
	} else {
		bool valid_device = false;
		if (source.compare("any") != 0) {
			pcap_if_t *alldevs = nullptr;
			pcap_if_t *d = nullptr;
			char errbuf[PCAP_ERRBUF_SIZE];

			/* Retrieve the device list from the local machine */
			if (pcap_findalldevs(&alldevs, errbuf) == -1) {
				std::ostringstream msg;
				msg << "Can not get list of network devices";

				AIWARN << msg.str();

				error_message(msg.str());
				exit(1);
			}

    			for(d = alldevs; d != nullptr; d = d->next) {
				if (source.compare(d->name) == 0) {
					valid_device = true;
					break;
				}
			}
			pcap_freealldevs(alldevs);
		} else
			valid_device = true;

		if (valid_device) {
			close_device();
			open_device(source, buffer_size);
		} else {
                	std::ostringstream msg;
                	msg << "Unknown device or file input:" << source.c_str();

			AIWARN << msg.str();

                	warning_message(msg.str());
		}
	}
}

void PacketDispatcher::run(void) {

	if (device_is_ready_) {
		run_device();
	} else {
		if (pcap_file_ready_)
			run_pcap();

                if ((http_acceptor_.is_open())and(io_running_ == false)) {
                        try {
                                status_ = PacketDispatcherStatus::RUNNING;
                                io_running_ = true;
                                io_service_.run();
                        } catch (const std::exception& e) {
                                std::ostringstream msg;
                                msg << ":ERROR:" << e.what() << std::endl;
                                msg << boost::diagnostic_information(e);

				AIERROR << msg.str();
                                error_message(msg.str());
                        }
                        status_ = PacketDispatcherStatus::STOPPED;
                }
	}
}

void PacketDispatcher::close(void) {

        if (device_is_ready_) {
                close_device();
        } else {
                if (pcap_file_ready_) {
                        close_pcap_file();
                }
        }
}

void PacketDispatcher::setPcapFilter(const char *filter) {

	if ((device_is_ready_)or(pcap_file_ready_)) {
		struct bpf_program fp;
		char *c_filter = const_cast<char*>(filter);

		if (pcap_compile(pcap_, &fp, c_filter, 1, PCAP_NETMASK_UNKNOWN) == 0) {
			pcap_filter_ = filter;
			if (pcap_setfilter(pcap_, &fp) == 0) {
				std::ostringstream msg;
                		msg << "Pcap filter set:" << filter;

                		info_message(msg.str());
			}
			pcap_freecode(&fp);
		} else {
			std::ostringstream msg;
			msg << "Wrong pcap filter";

			error_message(msg.str());
		}
	}
}

void PacketDispatcher::setEvidences(bool value) {

        if ((!have_evidences_)and(value)) {
                have_evidences_ = true;
                em_->enable();
        } else if ((have_evidences_)and(!value)) {
                have_evidences_ = false;
                em_->disable();
        }
}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING)

void PacketDispatcher::setShell(bool enable) {

        user_shell_->setShell(enable);
}

bool PacketDispatcher::getShell() const {

        return user_shell_->getShell();
}

void PacketDispatcher::setLogUserCommands(bool enable) {

	user_shell_->setLogUserCommands(enable);
}

bool PacketDispatcher::getLogUserCommands() const {

	return user_shell_->getLogUserCommands();
}

#endif

#if defined(LUA_BINDING)

void PacketDispatcher::setShell(lua_State *L, bool enable) {

        user_shell_->setShell(enable);
	user_shell_->setLuaInterpreter(L);
}

bool PacketDispatcher::getShell() const {

        return user_shell_->getShell();
}

#endif

#if defined(PYTHON_BINDING)

void PacketDispatcher::addTimer(PyObject *callback, int seconds) {

	tm_->addTimer(callback, seconds);
}

void PacketDispatcher::removeTimer(int seconds) {

	tm_->removeTimer(seconds);
}

#elif defined(RUBY_BINDING)
void PacketDispatcher::addTimer(VALUE callback, int seconds) {

	tm_->addTimer(callback, seconds);
}
#elif defined(LUA_BINDING)
void PacketDispatcher::addTimer(lua_State* L, const char *callback, int seconds) {

	tm_->addTimer(L, callback, seconds);
}
#endif

#if defined(PYTHON_BINDING)

void PacketDispatcher::showSystemProcessInformation() const {

	System system;

	system.statistics(OutputManager::getInstance()->out());
}

void PacketDispatcher::setStack(const boost::python::object &stack) {

	if (stack.is_none()) {
		// The user sends a Py_None
		pystack_ = boost::python::object();
		stack_name_ = "None";
        	defMux_.reset();
		eth_ = nullptr;
		current_network_stack_ = nullptr;
	} else {
		boost::python::extract<SharedPointer<NetworkStack>> extractor(stack);

        	if (extractor.check()) {
        		SharedPointer<NetworkStack> pstack = extractor();
                	pystack_ = stack;

			// The NetworkStack have been extracted and now call the setStack method
                	set_stack(pstack);
        	} else {
			std::ostringstream msg;

			msg << "Can not extract NetworkStack from python object";
			error_message(msg.str());
		}
	}
}

PacketDispatcher& PacketDispatcher::__enter__() {

	open(input_name_);
        return *this;
}

bool PacketDispatcher::__exit__(boost::python::object type, boost::python::object val, boost::python::object traceback) {

	close();
        return type.ptr() == Py_None;
}

void PacketDispatcher::forwardPacket(const std::string &packet, int length) {

	packet_ = reinterpret_cast<const uint8_t *>(packet.c_str());
	packet_length_ = length;
	// TODO: pass the time to the method forward_current_packet from the
	// python binding
	forward_current_ethernet_packet();
	return;
}

const char *PacketDispatcher::getStatus() const {

        if (status_ == PacketDispatcherStatus::RUNNING)
                return "running";
        else
                return "stoped";
}

void PacketDispatcher::addAuthorizedIPAddresses(const boost::python::list &items) {

        for (int i = 0; i < len(items); ++i) {
                // Check if is a std::string
                boost::python::extract<std::string> extractor(items[i]);
                if (extractor.check()) {
                        auto addr = extractor();

                        addAuthorizedIPAddress(addr);
                }
        }
}

boost::python::list PacketDispatcher::getAuthorizedIPAddresses() const {

        boost::python::list items;

        for (auto &it: allow_address_)
                items.append(it);

        return items;
}

#endif

std::ostream& operator<< (std::ostream &out, const PacketDispatcher &pd) {

	pd.statistics(out);
        return out;
}

void PacketDispatcher::statistics(std::basic_ostream<char> &out) const {

	struct pcap_stat stats;

	out << std::setfill(' ');
	out << "PacketDispatcher statistics\n";
	out << "\t" << "Connected to " << stack_name_ << "\n";
	if ((device_is_ready_)or(pcap_file_ready_))
		out << "\tCapturing from:         " << std::setw(10) << input_name_.c_str() << "\n";

	if (device_is_ready_) {
		std::string unit = "Bytes";
		int64_t buffer_size = pcap_buffer_size_;

		unitConverter(buffer_size, unit);

		out << "\t" << "Pcap buffer size:      " << std::setw(10 - (int)unit.length()) << buffer_size << " " << unit << "\n";
	}

        if (pcap_filter_.length() > 0)
                out << "\t" << "Pcap filter:" << pcap_filter_ << "\n";

	if ((device_is_ready_)and(pcap_stats(pcap_, &stats) == 0)) {
                out << "\t" << "Total received packets: " << std::dec << std::setw(10) << stats.ps_recv << "\n";
                out << "\t" << "Total dropped packets:  " << std::dec << std::setw(10) << stats.ps_drop << "\n";
                out << "\t" << "Total ifdropped packets:" << std::dec << std::setw(10) << stats.ps_ifdrop << "\n";
        }

        out << "\t" << "Total process packets:  " << std::dec << std::setw(10) << total_packets_ << "\n";
        out << "\t" << "Total bytes:        " << std::dec << std::setw(14) << total_bytes_ << "\n";

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)

	std::ostringstream shell_status;

        if (device_is_ready_) {
                // Compute the number of packets per second and bytes.
                int64_t packets_per_second = 0;
                int64_t bytes_per_second = 0;

                compute_packets_bytes_rate(packets_per_second, bytes_per_second);

                out << "\t" << "Total packets/sec:      " << std::dec << std::setw(10) << packets_per_second << "\n";
                out << "\t" << "Total bytes/sec:    " << std::dec << std::setw(14) << bytes_per_second << "\n";
        }

	if (user_shell_->getShell())
		shell_status << "enabled on " << ttyname(0);
	else
		shell_status << "disabled";

	out << "\tShell:" << std::setw(28) <<  shell_status.str() << "\n";

        tm_->statistics(out);

	if (http_acceptor_.is_open()) {
		int port = http_acceptor_.local_endpoint().port();
		std::ostringstream http_out;

		http_out << http_ip_address_.to_string() << ":" << port;
		out << "\t" << "HTTP server on:" << std::setw(19) << http_out.str() << "\n";
	}

	if (allow_address_.size() > 0) {
		out << "\t" << "Authorized IP address\n";
		for (auto &item: allow_address_)
			out << "\t\t" << item << "\n";
	}
#endif

        if (have_evidences_) {
		out << std::endl;
                em_->statistics(out);
        }
	out.flush();
}

void PacketDispatcher::statistics(Json &out) const {

	out["stack"] = stack_name_;
        if (device_is_ready_) {
		struct pcap_stat stats;

                out["device"] = input_name_.c_str();

		// Compute the number of packets per second and bytes.
		int64_t packets_per_second = 0;
		int64_t bytes_per_second = 0;

		compute_packets_bytes_rate(packets_per_second, bytes_per_second);

		if (pcap_stats(pcap_, &stats) == 0) {
			out["received packets"] = stats.ps_recv;
			out["dropped packets"] = stats.ps_drop;
			out["ifdropped packets"] = stats.ps_ifdrop;
		}

		out["buffer size"] = pcap_buffer_size_;
		out["packets/sec"] = packets_per_second;
		out["bytes/sec"] = bytes_per_second;
	} else if (pcap_file_ready_) {
		out["pcapfile"] = input_name_.c_str();
	}

	if (pcap_filter_.length() > 0)
		out["pcap_filter"] = pcap_filter_;

	out["process packets"] = total_packets_;
	out["bytes"] = total_bytes_;

	if (have_evidences_) {
		Json j;

		em_->statistics(j);

		out["evidences"] = j;
	}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
	out["shell"] = user_shell_->getShell();
#endif

}

void PacketDispatcher::status(void) {

	std::ostringstream msg;
        msg << "PacketDispatcher ";
	if (status_ == PacketDispatcherStatus::RUNNING)
		msg << "running";
	else
		msg << "stoped";
	msg << ", plug to " << stack_name_;
	msg << ", packets " << total_packets_ << ", bytes " << total_bytes_;

        info_message(msg.str());
}

void PacketDispatcher::showCurrentPayloadPacket(std::basic_ostream<char> &out) const {

	if ((device_is_ready_)or(pcap_file_ready_)) {
		if (current_network_stack_) {
			std::tuple<Flow*,Flow*> flows = current_network_stack_->getCurrentFlows();
			Flow *low_flow = std::get<0>(flows);
			Flow *high_flow = std::get<1>(flows);

			if (low_flow)
				out << "\tFlow:" << *low_flow << std::endl;

			if (high_flow)
				out << "\tFlow:" << *high_flow << std::endl;
		}
		if ((packet_)and(packet_header_))
			showPayload(out, packet_, packet_length_);

		out << std::dec;
	}
}

void PacketDispatcher::showCurrentPayloadPacket(Json &out) const {

        if ((device_is_ready_)or(pcap_file_ready_)) {
                if (current_network_stack_) {
                        std::tuple<Flow*,Flow*> flows = current_network_stack_->getCurrentFlows();
                        Flow *low_flow = std::get<0>(flows);
                        Flow *high_flow = std::get<1>(flows);

                        if (low_flow) {
				std::ostringstream flow_out;
				flow_out << *low_flow;

				if (high_flow)
					out["low_flow"] = flow_out.str();
				else
					out["flow"] = flow_out.str();
			}

                        if (high_flow) {
				std::ostringstream flow_out;
				flow_out << *high_flow;
                                out["high_flow"] = flow_out.str();
			}
                }
                if (packet_) {
                        std::vector<uint8_t> pkt;

                        for (int i = 0; i < packet_length_; ++i)
                                pkt.push_back(packet_[i]);

                        out["packet"] = pkt;
		}

        }
}

void PacketDispatcher::showCurrentPayloadPacket() const { 

	showCurrentPayloadPacket(OutputManager::getInstance()->out());
}

#if !defined(PYTHON_BINDING)

void PacketDispatcher::setStack(StackLan &stack) {

	SharedPointer<NetworkStack> s(&stack);
	set_stack(s);
}

void PacketDispatcher::setStack(StackMobile &stack) {

	SharedPointer<NetworkStack> s(&stack);
	set_stack(s);
}

void PacketDispatcher::setStack(StackLanIPv6 &stack) {

	SharedPointer<NetworkStack> s(&stack);
	set_stack(s);
}

void PacketDispatcher::setStack(StackVirtual &stack) {

	SharedPointer<NetworkStack> s(&stack);
	set_stack(s);
}

void PacketDispatcher::setStack(StackOpenFlow &stack) {

	SharedPointer<NetworkStack> s(&stack);
	set_stack(s);
}

void PacketDispatcher::setStack(StackMobileIPv6 &stack) {

	SharedPointer<NetworkStack> s(&stack);
	set_stack(s);
}

#endif

void PacketDispatcher::setHTTPPort(int port) {

        if (port > 0) {
                http_acceptor_.close();
                std::ostringstream msg;
                boost::asio::ip::tcp::endpoint admin_endpoint(http_ip_address_, port);

                http_acceptor_.open(admin_endpoint.protocol());
                http_acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
                http_acceptor_.bind(admin_endpoint);
                http_acceptor_.listen();

                accept_http_connections();
        } else {
                http_acceptor_.close();
        }
}

int PacketDispatcher::getHTTPPort() const {

        return http_acceptor_.local_endpoint().port();
}

void PacketDispatcher::addAuthorizedIPAddress(const std::string &addr) {

        struct sockaddr_in sa;

        if (inet_pton(AF_INET, addr.c_str(), &(sa.sin_addr)))
        	if (auto it = allow_address_.find(addr); it == allow_address_.end())
                	allow_address_.insert(addr);
}

void PacketDispatcher::accept_http_connections() {

        // Acceptor for http connections from administration pourposes
        http_acceptor_.async_accept(http_socket_, [&] (boost::beast::error_code ec) {
                if (!ec) {
                        [[maybe_unused]] std::time_t now = std::time(nullptr);
                        std::string ipaddress(http_socket_.remote_endpoint().address().to_string());

                        // Check if the ip address is allow to connect
                        if (auto it = allow_address_.find(ipaddress); it != allow_address_.end()) {

				if (current_network_stack_) {
                        		// Allocate space for the new administration connection
                                	auto conn = std::make_shared<HTTPSession>(std::move(http_socket_), current_network_stack_, this);

                                	// Start processing the HTTP
                                	conn->start();
				} else {
					AIWARN << "No stack plugged to the PacketDispatcher";
					http_socket_.close();
				}
                        } else {
				AIWARN << "IP addresss " << ipaddress << " not allowed";
                                http_socket_.close();
			}
                }
                accept_http_connections();
        });
}

void PacketDispatcher::compute_packets_bytes_rate(int64_t &packets_per_second, int64_t &bytes_per_second) const {

	int seconds = difftime(stats_.packet_time, stats_.last_packet_time);
        packets_per_second = total_packets_ - stats_.last_total_packets_sample;
        bytes_per_second = total_bytes_ - stats_.last_total_bytes_sample;

        if (seconds > 0 ) {
        	packets_per_second = packets_per_second / seconds;
                bytes_per_second = bytes_per_second / seconds;
	}

        stats_.last_packet_time = stats_.packet_time; // update the last time we make the compute
        stats_.last_total_packets_sample = total_packets_;
        stats_.last_total_bytes_sample = total_bytes_;
}

int64_t PacketDispatcher::getTotalReceivedPackets(void) const {

	struct pcap_stat stats;
	int64_t value = 0;

	if ((device_is_ready_)and(pcap_stats(pcap_, &stats) == 0))
                value = stats.ps_recv;

	return value;
}
 
int64_t PacketDispatcher::getTotalDroppedPackets(void) const {

	struct pcap_stat stats;
	int64_t value = 0;

	if ((device_is_ready_)and(pcap_stats(pcap_, &stats) == 0))
                value = stats.ps_drop;

	return value;
}

int64_t PacketDispatcher::getTotalIfDroppedPackets(void) const {

	struct pcap_stat stats;
	int64_t value = 0;

	if ((device_is_ready_)and(pcap_stats(pcap_, &stats) == 0))
                value = stats.ps_ifdrop;

	return value;
}

} // namespace aiengine
